import { combineReducers } from "redux";
import api, { addAuth } from "../../utils/api";
import thunk from "redux-thunk";

const initialState = {
  isModalShowing: null,
  isLoading: false,
  token: null,
  error: null,
  user: null,
};

const state = {
  isLoading: (state = initialState.isLoading, action) => {
    if (action.type === "TOGGLE_AUTH_ISLOADING") return state ? false : true;
    else return state;
  },
  token: (state = initialState.token, action) => {
    switch (action.type) {
      case "SET_AUTH_TOKEN":
        return action.payload;
      case "CLEAR_AUTH_TOKEN":
        return null;
      default:
        return state;
    }
  },

  error: (state = initialState.error, action) => {
    switch (action.type) {
      case "SET_AUTH_ERROR":
        return action.payload;
      case "CLEAR_AUTH_ERROR":
        return null;
      default:
        return state;
    }
  },
  user: (state = null, action) => {
    switch (action.type) {
      case "SET_AUTH_USER":
        return action.payload;
      case "CLEAR_AUTH_USER":
        return null;
      default:
        return state;
    }
  },
  isModalShowing: (state = initialState.isModalShowing, action) => {
    switch (action.type) {
      case "TOGGLE_IS_MODAL_SHOWING":
        return action.payload ? action.payload : null;
      default:
        return state;
    }
  },
};

const authReducer = combineReducers(state);

export default authReducer;
