import { Component, OnInit } from "@angular/core";
import { BookService } from "src/app/services/book.service";
import { HomeService } from "src/app/services/home.service";

@Component({
  selector: "app-home-page",
  templateUrl: "./home-page.component.html",
  styleUrls: ["./home-page.component.scss"],
})
export class HomePageComponent implements OnInit {
  data: any;
  feature: any;
  constructor(private homeService: HomeService) {}

  ngOnInit() {
    this.getHeader();
    this.getFeature();
  }

  getHeader() {
    console.log("allala");
    this.homeService.getHeader().subscribe((res) => {
      this.data = res;
      console.dir(this.data);
    });
  }
  getFeature() {
    console.log("allala");
    this.homeService.getFeature().subscribe((res: any) => {
      this.feature = res.data;
      console.dir(this.feature);
    });
  }
}
