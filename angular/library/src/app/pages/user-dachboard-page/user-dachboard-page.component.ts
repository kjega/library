import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-user-dachboard-page",
  templateUrl: "./user-dachboard-page.component.html",
  styleUrls: ["./user-dachboard-page.component.scss"],
})
export class UserDachboardPageComponent implements OnInit {
  currentUser: any;
  currentUserID: number;
  constructor(private router: Router) {
    this.currentUser = this.router.getCurrentNavigation().extras.state.currentuser;
    this.currentUserID = this.currentUser.data.user.id;
    console.dir(this.currentUserID);
  }

  ngOnInit() {}
}
