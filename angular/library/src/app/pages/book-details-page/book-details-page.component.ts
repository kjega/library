import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { BookService } from "src/app/services/book.service";

@Component({
  selector: "app-book-details-page",
  templateUrl: "./book-details-page.component.html",
  styleUrls: ["./book-details-page.component.scss"],
})
export class BookDetailsPageComponent implements OnInit {
  bookId: number;
  currentUser: any;
  bookDetail: any;
  constructor(private router: Router, private bookService: BookService) {
    this.bookId = this.router.getCurrentNavigation().extras.state.id;
  }

  ngOnInit() {
    this.getCurrentUser();
    this.getBookDetail(this.bookId);
  }

  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    return JSON.parse(localStorage.getItem("currentUser"));
  }
  getBookDetail(bookId) {
    return this.bookService.getBookDetail(bookId).subscribe((res: any) => {
      this.bookDetail = res.data[0];
      console.dir(this.bookDetail);
    });
  }
}
