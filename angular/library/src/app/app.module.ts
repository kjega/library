import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HomePageComponent } from "./pages/home-page/home-page.component";
import { BooksPageComponent } from "./pages/books-page/books-page.component";
import { BookDetailsPageComponent } from "./pages/book-details-page/book-details-page.component";
import { UserDachboardPageComponent } from "./pages/user-dachboard-page/user-dachboard-page.component";
import { AdminDachboardPageComponent } from "./pages/admin-dachboard-page/admin-dachboard-page.component";
import { NavComponent } from "./components/nav/nav.component";
import { FooterComponent } from "./components/footer/footer.component";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { ModalLoginComponent } from "./components/modal-login/modal-login.component";
import { ModalRegisterComponent } from "./components/modal-register/modal-register.component";
import { NgbActiveModal, NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ReactiveFormsModule } from "@angular/forms";
import { BooksUserComponent } from './components/books-user/books-user.component';
import { BooksGuestComponent } from './components/books-guest/books-guest.component';
import { ModelAddBookComponent } from './components/model-add-book/model-add-book.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    BooksPageComponent,
    BookDetailsPageComponent,
    UserDachboardPageComponent,
    AdminDachboardPageComponent,
    NavComponent,
    FooterComponent,
    ModalLoginComponent,
    ModalRegisterComponent,
    BooksUserComponent,
    BooksGuestComponent,
    ModelAddBookComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    ReactiveFormsModule,
  ],
  providers: [HttpClient, NgbActiveModal],
  bootstrap: [AppComponent],
  entryComponents: [ModalLoginComponent, ModalRegisterComponent],
})
export class AppModule {}
