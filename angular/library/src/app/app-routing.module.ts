import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomePageComponent } from "./pages/home-page/home-page.component";
import { BooksPageComponent } from "./pages/books-page/books-page.component";
import { UserDachboardPageComponent } from "./pages/user-dachboard-page/user-dachboard-page.component";
import { BookDetailsPageComponent } from "./pages/book-details-page/book-details-page.component";
import { ModelAddBookComponent } from "./components/model-add-book/model-add-book.component";
import { AdminDachboardPageComponent } from "./pages/admin-dachboard-page/admin-dachboard-page.component";

const routes: Routes = [
  { path: "", component: HomePageComponent },
  { path: "books", component: BooksPageComponent },
  { path: "books/:id", component: BookDetailsPageComponent },
  { path: "user/:id", component: UserDachboardPageComponent },

  {
    path: "admin/dashboard",
    component: AdminDachboardPageComponent,
    children: [
       { path: "admin/addBook", component: ModelAddBookComponent }],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
