import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { catchError, mapTo, tap } from "rxjs/operators";
import { Router } from "@angular/router";

@Injectable({
  providedIn: "root",
})
export class UserService {
  apiURL = "http://localhost:8000/api/user/";
  constructor(private http: HttpClient, private router: Router) {}
  private;
  register(data) {
    return this.http.post(`${this.apiURL}register`, data);
  }

  connect(connectUser) {
    console.dir(connectUser);
    return this.http.post(`${this.apiURL}authenticate`, connectUser).pipe(
      tap((user) => this.doLogin(user)),
      mapTo(true),
      catchError((err) => err)
    );
  }

  doLogin(user: any) {
    localStorage.setItem("currentUser", JSON.stringify(user));
    this.router.navigate(["/userDashboard"]);
    // this.isLoggedIn();
  }
  logOut() {
    localStorage.removeItem("currentUser");
  }
}
