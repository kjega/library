import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class BookService {
  apiURL = "http://localhost:8000/api/book/";
  constructor(private http: HttpClient) {}

  getAllBooks() {
    return this.http.get(`${this.apiURL}`);
  }
  getBookDetail(id) {
    return this.http.get(`${this.apiURL}${id}`);
  }
  getAllCategory() {
    return this.http.get(`${this.apiURL}categories`);
  }
}
