import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class AuthorService {
  apiURL = "http://localhost:8000/api/author/";
  constructor(private http: HttpClient) {}

  getAuthors() {
    return this.http.get(`${this.apiURL}`);
  }
}
