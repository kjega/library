import { Component, OnInit } from "@angular/core";
import { ModalRegisterComponent } from "../modal-register/modal-register.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalLoginComponent } from "../modal-login/modal-login.component";
import { UserService } from "src/app/services/user.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-nav",
  templateUrl: "./nav.component.html",
  styleUrls: ["./nav.component.scss"],
})
export class NavComponent implements OnInit {
  currentUser: any;
  constructor(
    private modalService: NgbModal,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    console.dir(this.currentUser);
    console.dir(this.currentUser.data.user.role);
    return JSON.parse(localStorage.getItem("currentUser"));
  }

  showInscriptionModal() {
    this.modalService.open(ModalRegisterComponent);
  }

  showConnectionModal() {
    const modalRef = this.modalService.open(ModalLoginComponent);
    modalRef.result.then((result) => {
      this.currentUser = result;
    });
  }

  logout() {
    this.userService.logOut();
    this.getCurrentUser();
  }
  goUserSpace(currentuser) {
    const userId = currentuser.data.user.id;
    this.router.navigate(["user/" + userId], { state: { currentuser } });
  }
}
