import { Component, OnInit, Input } from "@angular/core";
import { BookService } from "src/app/services/book.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-books-guest",
  templateUrl: "./books-guest.component.html",
  styleUrls: ["./books-guest.component.scss"],
})
export class BooksGuestComponent implements OnInit {
  allBooks: any;
  allCategory: [];
  constructor(private booksService: BookService, private router: Router) {}

  ngOnInit() {
    this.getBooks();
    this.getAllCategory();
  }
  getBooks() {
    this.booksService.getAllBooks().subscribe((res: any) => {
      this.allBooks = res.data;
      console.dir(this.allBooks);
    });
  }
  goByBookId(id) {
    this.router.navigate(["books/" + id], { state: { id } });
  }
  getAllCategory() {
    this.booksService.getAllCategory().subscribe((res: any) => {
      this.allCategory = res.data;
      console.dir(this.allCategory);
    });
  }
}
