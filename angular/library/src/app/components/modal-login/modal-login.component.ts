import { Component, OnInit } from "@angular/core";
import {
  Validators,
  FormBuilder,
  FormGroup,
  FormControl,
} from "@angular/forms";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-modal-login",
  templateUrl: "./modal-login.component.html",
  styleUrls: ["./modal-login.component.scss"],
})
export class ModalLoginComponent implements OnInit {
  currentUser;
  // @Input() public user;

  FormConnection: FormGroup = this.fb.group({
    email: ["", Validators.compose([Validators.required, Validators.email])],
    password: [
      "",
      Validators.compose([Validators.required, Validators.minLength(6)]),
    ],
  });
  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private userService: UserService
  ) {}

  ngOnInit() {}

  connect() {
    // console.dir(this.FormConnection.value);
    this.userService.connect(this.FormConnection.value).subscribe(
      (data) => {
        this.getCurrentUser();
        this.closeModal(this.currentUser);
      },
      (err) => {
        console.dir(err);
      }
    );
  }

  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem("currentUser"));
    return JSON.parse(localStorage.getItem("currentUser"));
  }

  closeModal(currentUser) {
    this.activeModal.close(currentUser);
  }
}
