import { Component, OnInit } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UserService } from "src/app/services/user.service";

@Component({
  selector: "app-modal-register",
  templateUrl: "./modal-register.component.html",
  styleUrls: ["./modal-register.component.scss"],
})
export class ModalRegisterComponent implements OnInit {
  FormRegister: FormGroup = this.fb.group({
    username: ["", Validators.compose([Validators.required])],
    email: ["", Validators.compose([Validators.required, Validators.email])],
    password: [
      "",
      Validators.compose([Validators.required, Validators.minLength(6)]),
    ],
  });

  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private userService: UserService
  ) {}

  ngOnInit() {}
  register() {
    console.dir(this.FormRegister.value);
    this.userService.register(this.FormRegister.value).subscribe(
      (data) => {
        console.dir(data);
        this.closeModal();
      },
      (err) => {
        console.dir(err);
      }
    );
  }

  closeModal() {
    this.activeModal.close();
  }
}
