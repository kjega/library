import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { BookService } from "src/app/services/book.service";
import { Router } from "@angular/router";
import { AuthorService } from "src/app/services/author.service";

@Component({
  selector: "app-model-add-book",
  templateUrl: "./model-add-book.component.html",
  styleUrls: ["./model-add-book.component.scss"],
})
export class ModelAddBookComponent implements OnInit {
  FormAddBook: FormGroup = this.fb.group({
    title: ["", Validators.compose([Validators.required])],
    resume: ["", Validators.compose([Validators.required])],
    imgUrl: ["", Validators.compose([Validators.required])],
    author: ["", Validators.compose([Validators.required])],
    category: ["", Validators.compose([Validators.required])],
    date: ["", Validators.compose([Validators.required])],
  });

  allBooks: any;
  allCategory: [];
  allAuthors: [];
  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private booksService: BookService,
    private authorService: AuthorService
  ) {}

  ngOnInit() {
    this.getAuthors();
    this.getAllCategory();
  }

  addBook() {}

  closeModal() {
    this.activeModal.close();
  }
  getAllCategory() {
    this.booksService.getAllCategory().subscribe((res: any) => {
      this.allCategory = res.data;
      console.dir(this.allCategory);
    });
  }
  getAuthors() {
    this.authorService.getAuthors().subscribe((res: any) => {
      this.allAuthors = res.data;
      console.dir(this.allAuthors);
    });
  }
}
